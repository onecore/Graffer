__author__ = ['Mark Anthony R. Pequeras']
__language__ = ['Python', 'Kivy', 'Cython', 'C']
__version__ = 0.1
__distributor__ = ['CoreSEC Softwares, CA']
__license__ = ['MIT']
__website__ = 'http://www.coresecsoftware.com/markie'
__app__ = 'Graffer - The Best Graffiti Studio'

from kivy.uix.widget import Widget
from kivy.graphics import *
from kivy.app import runTouchApp,App
from kivy.uix.floatlayout import FloatLayout
from kivy.core.audio import SoundLoader
from kivy.uix.scatter import Scatter
from kivy.uix.colorpicker import Color, ColorPicker,ColorWheel
from kivy.uix.image import Image
from kivy.parser import parse_color
from kivy.clock import Clock
from functools import partial
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.modules import touchring
from kivy.properties import *
from Data.libs import gcolors
from kivy.lang import Builder
from kivy.animation import Animation


'''
Graffer is Created for my favorite graffiti software (Graffiti Studio)
Alternative, where you can draw Graffitis on different canvases with your
Mice or Device souch as touch pad etc.
'''


global Caches

Caches = {
    "color":(1,0,1,1),  #// Paint Color
    "sound_state":True, #// On/Off
    "cap_state":0,  #// Down/Up
    "cap_size":10,  #// Brush Size
    "cap_opacity":.9, #// Opacity (Brush not Canvas)
    "res_path":"Data/resources/", #// Resources Path
    "canvas_path":"Data/resources/canvas/", #// Canvas Path
    "caps_path":"Data/resources/caps/", #// Caps path (PNGs)
    "font":"Data/resources/fontawesome.ttf", #// Unicode Fonts/Icons
    "brush_type":23, #// Opacity
    "redo":False, #// Redo
    "undo":False, #// Undo
    "brush":True, #// Can/Marker

}


class GrafferLogEngine(object):
    ''' Graffer Offline/Online Class Engine Graffer API'''
    def __init__(self):
        pass

    def passed(self):
        return True

class GrafferCan(Image):
    def __init__(self,**kwargs):
        super(GrafferCan,self).__init__(**kwargs)
        self.brushes = {0:Caches['res_path']+'g_can.png',1:Caches['res_path']+'g_can.png'}
        self.source = Caches['res_path']+'g_can.png'
        self.size_hint=(.2,.2)

    def Switch(self):
        '''
        Switches the neither brush/marker
        '''
        global Caches

        if Caches['brush']:
            self.source = self.brushes[Caches['brush']]
            Caches['brush'] = False
        else:
            self.source = self.brushes[Caches['brush']]
            Caches['brush'] = True

        print "switched to ", self.source



class GrafferCanvas(Image):
    ''' Art Canvas Processing '''
    _past = ()
    pass


class GrafferEngine(Scatter):
    ''' Events and Processing Class '''
    def __init__(self, **kwargs):
            # make sure we aren't overriding any important functionality
        super(GrafferEngine, self).__init__(**kwargs)
        texture = Image(source=Caches['canvas_path']+'walls/1.jpg').texture
        self.G_Canvas = Rectangle(texture=texture, pos=self.pos, size=self.size)
        self.G_Paint = Line(cap_precision=23,cap='round',width=Caches['cap_size'])


        #// KW as callbacks

    def g_sound(self,shake=False,stop=False):
        '''
        Plays the defined sound in App
        '''
        print 'sound play()'
        gr_shake = None
        if Caches['sound_state']:
            gr_shake = SoundLoader.load(Caches['res_path']+'g_shake.mp3')
            gr_shake.seek(6)
            gr_shake.play()

        else:
            try:
                gr_shake.unload()
            except:
                pass


    def calculate_points(self,x1, y1, x2, y2, steps=7):
        from math import sqrt
        dx = x2 - x1 # cursor pos
        dy = y2 - y1
        dist = sqrt(dx * dx + dy * dy)

        if dist < steps:
            return None

        o = []
        m = dist / steps
        for i in range(1, int(m)): #// 2 change point
            mi = i / m  # Dir
            lastx = x1 + dx * mi
            lasty = y1 + dy * mi
            o.extend([lastx, lasty])

        return o

    def canvas_move(self,dir=None):
        pass
    
    def on_touch_up(self, touch):
        if touch.grab_current is not self:
            return

        touch.ungrab(self)
        ud = touch.ud
        self.canvas.remove_group(ud['group'])

    def on_touch_down(self, touch): #// Spray Triggered
        print "on_touch_down"
        global Caches
        self.canvas.clear()
        self.G_Canvas.pos = self.pos
        self.G_Canvas.size = self.size
        self.canvas.add(self.G_Canvas)
        touch.ud['group'] = g = str(touch.uid)

        with self.canvas.after:
            if Caches['brush']:
                Color(*Caches['color'], group=g)
                touch.ud['line'] = Point(points=(touch.x,touch.y),
                                source='Data/resources/caps/default/4.png',
                                pointsize=Caches['cap_size'])
            else:
                print "Marker"

        touch.grab(self)
        return True

    def on_touch_move(self, touch): #// Spray Trigger + Move
        if touch.grab_current is not self:
            return

        ud = touch.ud

        ud['line'].add_point(touch.x,touch.y)

class GrafferMain(App):
    ''' Main Graffer Class '''
    def __init__(self, **kwargs):
        super(GrafferMain, self).__init__(**kwargs)
        #Clock.schedule_interval(partial(self.g_mouseobj), 0.05) #// For Operating the Spray can via Cursor Position.
        self._transparent = (0,0,0,0)
        self._fontname = Caches['font']
        self._fontcolor = parse_color("#989898")
        self._fontsize = 25
        self._pos = lambda x,y: {"center_x":x,"center_y":y}



    def g_sizechange_1(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 37
        _u.on_release = change
        _u.size_hint = .1,.1
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 60
        _u.pos_hint = self._pos(.96,.6)
        return _u

    def g_sizechange_2(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 25

        _u.on_release = change
        _u.size_hint = .1,.1
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 50
        _u.pos_hint = self._pos(.964,.51)
        return _u

    def g_sizechange_3(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 18

        _u.on_release = change
        _u.size_hint = .1,.1
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 40
        _u.pos_hint = self._pos(.969,.435)
        return _u

    def g_sizechange_4(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 10

        _u.on_release = change
        _u.size_hint = .1,.1
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 31
        _u.pos_hint = self._pos(.973,.37)
        return _u

    def g_sizechange_5(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 7
        _u.on_release = change
        _u.size_hint = .1,.1
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 20
        _u.pos_hint = self._pos(.976,.323)
        return _u

    def g_sizechange_6(self):
        global Caches
        _u = Button()
        _u.text = u'\uf111'

        def change():
            global Caches
            Caches['cap_size'] = 2

        _u.on_release = change
        _u.size_hint = .05,.05
        _u.opacity = .6
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = 12
        _u.pos_hint = self._pos(.977,.284)
        return _u
    Geom_X = lambda v,x: (v+x(.4/v))

    def g_sizechange_custom(self):
        pass


    def g_volumeobj(self):
        '''Volume Object'''
        _status = {0:u"\uf028", 1:u"\uf026"}
        global Caches
        _s = Button()

        def changeStatus():
            global Caches
            if Caches['sound_state']:
                Caches['sound_state'] = False
                _s.text = _status[1]
            else:
                _s.text = _status[0]
                Caches['sound_state'] = True

        _s.on_press = changeStatus
        _s.size_hint = .1,.1
        _s.background_color = self._transparent
        _s.font_name = self._fontname
        _s.text = _status[0]
        _s.font_size = self._fontsize
        _s.pos_hint = self._pos(.97,.04)
        return _s

    def g_userobj(self):
        '''User management Object
        - Contains Logging in/out
        - Creation uploading to site
        - Viewing histories
        - Getting online Rankings
        '''

        _u = Button()
        _u.text = u'\uf007'

        def showpanel():
            'Show Login Panel etc.'
            print "panelshow"

        _u.on_release = showpanel
        _u.size_hint = .1,.1
        _u.background_color = self._transparent
        _u.font_name = self._fontname
        _u.font_size = self._fontsize
        _u.pos_hint = self._pos(.96,.96)
        return _u

    def g_paintcolorobj(self):
        global Caches

        def CParser(instance):
            global Caches
            Selected = instance.background_color
            Caches['color'] = Selected
            print Selected


        Scroller = GridLayout(cols=1, size_hint_y=None)
        Scroller.bind(minimum_height=Scroller.setter('height'))
        for _,hx in gcolors.MixedColors.items():
            btn = Button(size_hint_y=None, height=30,size_hint_x=.1)
            btn.background_color = parse_color(hx)
            btn.bind(on_press=CParser)
            Scroller.add_widget(btn)

        SV = ScrollView(size_hint=(.1, 1),
            pos_hint={'center_x':.01, 'center_y':.5})

        SV.add_widget(Scroller)
        return SV

    def g_mouseobj(self,dt):
        'Automatically move the can into curpos.'
        _mousePos = Window.mouse_pos
        _x,_y =_mousePos
        zoom_3 = _x-123,_y-169
        zoom_2 = _x-123,_y-169
        zoom_1 = _x-123,_y-169
        self.MouseObj.pos = _x-80,_y-113


    def build(self):


        self.Main = FloatLayout() #// Layout Handler for Widgets
        self.title = __app__
        self.MouseObj = GrafferCan()
        self.Main.add_widget(GrafferEngine())

        #// Contents
        self.Main.add_widget(self.g_volumeobj())
        self.Main.add_widget(self.g_userobj())

        self.Main.add_widget(self.g_sizechange_1())
        self.Main.add_widget(self.g_sizechange_2())
        self.Main.add_widget(self.g_sizechange_3())
        self.Main.add_widget(self.g_sizechange_4())
        self.Main.add_widget(self.g_sizechange_5())
        self.Main.add_widget(self.g_sizechange_6())
        self.Main.add_widget(self.g_paintcolorobj())


        #// Contents
        #self.Main.add_widget(self.MouseObj)

        return self.Main


if __name__ == '__main__':
    if GrafferLogEngine().passed():
        GrafferMain().run()